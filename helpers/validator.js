const { validationResult } = require('express-validator');

module.exports.handleValidation = (validations) => async (req, res, next) => {
  try {
    await Promise.all(validations.map((validation) => validation.run(req)));

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    return next();
  } catch (err) {
    return next(err);
  }
};
