const express = require('express');
const { DateTime } = require('luxon');
const { isEmpty } = require('ramda');
const { check } = require('express-validator');
const { asyncMiddleware } = require('../helpers/async');
const { handleValidation } = require('../helpers/validator');
const { isWithinAnHourAgo, getSum } = require('../helpers/filters');

const router = express.Router();
let store = [];

router.post('/:key', handleValidation([check('value').isNumeric()]), asyncMiddleware(async (req, res) => {
  const newValue = {
    createdAt: DateTime.local().toISO(),
    key: req.params.key,
    value: req.body.value,
  };
  store = store.concat([newValue]);

  return res.send({});

}));

router.get('/:key/sum', asyncMiddleware(async (req, res) => {
  const { key } = req.params;
  const currentDateTimeLocal = DateTime.local();

  // filter and reset store to only include values within the past hour
  store = store.filter(({ createdAt }) => isWithinAnHourAgo(currentDateTimeLocal, DateTime.fromISO(createdAt)));

  const keyValues = store.filter((keyValue) => keyValue.key === key);

  if (isEmpty(keyValues)) return res.send({ value: 0 });

  const total = getSum(keyValues.map(({ value }) => value));

  return res.send({
    value: total,
  });

}));

module.exports = router;
