const { DateTime } = require('luxon');
const { expect } = require('chai');
const { isWithinAnHourAgo, getSum } = require('../helpers/filters');

const numberValues = [1, 2, 3, 4, 5, 6, 7, 8];
const decimalNumberValues = [1, 2.2, 3.4];

const isoTimes = [
  '2020-01-31T03:41:58.289-06:00',
  '2020-01-31T04:41:58.289-06:00',
  '2020-01-31T05:21:58.289-06:00',
  '2020-01-31T05:31:58.289-06:00',
  '2020-01-31T05:40:58.289-06:00',
];

const currentIsoTime = '2020-01-31T05:41:58.289-06:00';

describe('Filters', () => {
  describe('isWithinAnHourAgo()', () => {
    it('should return 3 values within the previous hour', () => {
      const currentDateTime = DateTime.fromISO(currentIsoTime);

      const timesWithinRange = isoTimes
        .filter((isoTime) => isWithinAnHourAgo(currentDateTime, DateTime.fromISO(isoTime)));

      expect(timesWithinRange).to.have.lengthOf(3);
    });
    it('should return 2 values outside of the previous hour', () => {
      const currentDateTime = DateTime.fromISO(currentIsoTime);

      const timesWithinRange = isoTimes
        .filter((isoTime) => !isWithinAnHourAgo(currentDateTime, DateTime.fromISO(isoTime)));

      expect(timesWithinRange).to.have.lengthOf(2);
    });
  });

  describe('getSum()', () => {
    it('should return 36 when doing a summation', () => {
      const summation = getSum(numberValues);

      expect(summation).to.equal(36);
    });
    it('should round decimals', () => {
      const summation = getSum(decimalNumberValues);

      expect(summation).to.equal(6);
    });
  });
});
