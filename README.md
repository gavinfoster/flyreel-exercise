# Getting Started

## Requirements

- NOTE: Requires Node >= v10

## Running Project

`npm install`

`npm start`

Reloading for dev
`npm run dev`

## Tests

`npm test`

## Linting

`npm run lint`

`npm run lint -- --fix`

## Env Vars

`cp .env.example .env`

## Project Notes
- Added some inline comments re: thoughts (things I wouldn't typically put in a codebase)
- Wasn't 100% sure about rounding as part of summation, but just went with what made sense to me
- I've been writing more typescript recently and think it's valuable, but in this case I wasn't sure if that was within the bounds of this
- Tried to respect the short timeframe of the exercise ~50 min, so things like security, documentation, extensive error handling, validation, sanitization etc. aren't a part of this at all