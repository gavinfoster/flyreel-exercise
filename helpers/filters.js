module.exports.isWithinAnHourAgo = (currentDateTimeLocal, dateTimeValue) => currentDateTimeLocal
  .minus({ hours: 1 }) < dateTimeValue;

module.exports.getSum = (array) => array.reduce((total, num) => total + Math.round(num), 0);
