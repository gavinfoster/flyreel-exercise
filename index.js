require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');

const server = express();
const metric = require('./routes/metric');

const port = process.env.PORT || 8080;

server.use(bodyParser.json());
// TODO: this should probably be /metrics
// https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design
server.use('/metric', metric);

// eslint-disable-next-line no-console
server.listen(port, () => console.log(`App listening on port ${port}!`));
